<?php
// validamos si ya existe un login activo
session_start();
if (isset($_SESSION['nombre'])) {
    header('Location: ../index.php');
    exit;
}

// validamos los diferentes errores de login que se pueden generar
$mensaje = '';
if(isset($_GET['error'])){
    if($_GET['error'] == 1){
        $mensaje = "El usuario no existe en nuestra base de datos";
    }elseif($_GET['error'] == 2){
        $mensaje = "Correo invalido";
    }elseif($_GET['error'] == 3){
        $mensaje = "Contraseña invalida";
    }elseif($_GET['error'] == 4){
        $mensaje = "Usuario inactivo por excesos de intentos de logueo";
    }elseif($_GET['error'] == 5){
        $mensaje = "Usuario Inactivo";
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link href="../css/login.css" rel="stylesheet">
</head>
    <body class="text-center">
        <main class="form-signin w-100 m-auto">
        <form action="validar_datos_login.php" method="post">
            <h1 class="h3 mb-3 fw-normal">Login</h1>

            <?php if($mensaje != ''){ ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $mensaje;?><br>
                </div>
            <?php } ?>

            <div class="form-floating">
            <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com">
            <label for="floatingInput">Email o Correo Electrónico</label>
            </div>
            <div class="form-floating">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            <label for="floatingPassword">Contraseña</label>
            </div>

        
            <button class="w-100 btn btn-lg btn-primary" type="submit">Iniciar sesión</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2023</p>
        </form>
        </main>
    </body>

</html>