<?php
include_once('../globales/conexion.php');

session_start();

if( array_key_exists( 'cuenta_intentos', $_SESSION ) ){
    $_SESSION['cuenta_intentos']++;
} else {
    $_SESSION['cuenta_intentos'] = 0;
}

$email = $_POST["email"];
$password = trim($_POST["password"]);

$email = stripcslashes($email);  
$password = stripcslashes($password);  
$email = mysqli_real_escape_string($conn, $email);  
$password = mysqli_real_escape_string($conn, $password);  

$password = md5($password);

$query = "select * from usuarios where lower(email) = '".trim(mb_strtolower($email))."'";

$ejecutaQuery = $conn->query($query);
// sin datos consultados
if(mysqli_num_rows($ejecutaQuery) == 0){
    session_unset();
    session_destroy();
    header('location: login.php?error=1');
    $conn->close();
    exit;
}

$fila = $ejecutaQuery->fetch_object();

// correo invalido
if($fila->email == ''){
    session_unset();
    session_destroy();
    header('location: login.php?error=2');
    $conn->close();
    exit;
}

// si los intentos de login superan los 3 entonces lo inactivamos y cerramos el proceso de login en el sistema
if($_SESSION['cuenta_intentos'] >= 3){
    session_unset();
    session_destroy();
    $ejecutaQuery = $conn->query("update usuarios set id_estado_general = 3 where id = $fila->id");
    header('location: login.php?error=4');
    $conn->close();
    exit;
}

// password invalido
if($fila->password != $password){
    $_SESSION['cuenta_intentos']++;
    header('location: login.php?error=3');
    $conn->close();
    exit;
}

// usuario inactivo por exceso de intentos de logueo
if($fila->id_estado_general == '3'){
    $_SESSION['cuenta_intentos']++;
    header('location: login.php?error=4');
    $conn->close();
    exit;
}

// usuario inactivo
if($fila->id_estado_general == '2'){
    $_SESSION['cuenta_intentos']++;
    header('location: login.php?error=5');
    $conn->close();
    exit;
}


$_SESSION['id'] = $fila->id;
$_SESSION['nombre'] = $fila->nombre;
$_SESSION['email'] = $fila->email;
$_SESSION['id_estado'] = $fila->id_estado;
$_SESSION['login'] = true;

header('Location: ../index.php');

?>