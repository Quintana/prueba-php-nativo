## Prueba PHP - NATIVO

Para poder inicializar el siguiente proyecto se debe realizar la siguiente configuracion

1. Tener instalado XAMPP o WAMP en su PC
2. Importar la base de datos ubicada en la carpeta "base_de_datos" la cual ya contiene informacion de pruebas
3. Clonar el proyecto dentro de la carpeta www o htdocs segun el programa intalado en el primer punto
4. Los usuarios que estan creados tienen una encriptacion MD5 en su contraseña por lo que las contraseñas de cada usuario es su primer nombre con la primer letra en mayuscula y el resto en minuscula seguido de .123 ej.: Darwin.123, Allison.123, Erick.123 y Magda.123
5. la conexion a la base de datos esta en el archivo ubicado en la ruta "globales/conexion.php" ya que es importante que configuren o creen el nombre de la base de datos segun estos parametros para no tener ningun problema con dicha conexion

