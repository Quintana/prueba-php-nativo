<?php
include_once('../globales/conexion.php');
include_once('../globales/valida_usuario_auth.php');

$data = [];

$complemento = "";

if($_POST['fecha_inicio'] != '' || $_POST['fecha_fin'] != '' || $_POST['nombre_solicitante_filtro'] != '' || $_POST['asunto_filtro'] != '' || $_POST['usuario_creo_filtro'] != ''){

    $_POST['nombre_solicitante_filtro'] != '' ? $_POST['nombre_solicitante_filtro'] = "like '%".trim(mb_strtolower($_POST['nombre_solicitante_filtro']))."%'" : "like ''";
    $_POST['asunto_filtro'] != '' ? $_POST['asunto_filtro'] = "like '%".trim(mb_strtolower($_POST['asunto_filtro']))."%'" : "like ''";

    $complemento = "where (DATE_FORMAT(a.fecha, '%Y-%m-%d') BETWEEN '".$_POST['fecha_inicio']."' and '".$_POST['fecha_fin']."')
                    or lower(a.nombre_solicitante) ".$_POST['nombre_solicitante_filtro']."
                    or lower(a.asunto) ".$_POST['asunto_filtro']."
                    or a.id_usuario = '".$_POST['usuario_creo_filtro']."'";
}

$query = "SELECT a.id, b.nombre as usuario_creador, a.nombre_solicitante as solicitante, a.fecha, a.asunto, a.texto_solicitud 
          FROM radicaciones as a
          inner join usuarios as b on a.id_usuario = b.id
          $complemento
          order by a.id desc";

$ejecutaQuery = $conn->query($query);

while($fila = $ejecutaQuery->fetch_object()){
    $data[] = [
        "id" => $fila->id,
        "usuario_creador" => $fila->usuario_creador,
        "solicitante" => $fila->solicitante,
        "fecha" => $fila->fecha,
        "asunto" => $fila->asunto,
        "texto_solicitud" => $fila->texto_solicitud
    ];
}

$data = ['data' => $data];

echo json_encode($data);
?>