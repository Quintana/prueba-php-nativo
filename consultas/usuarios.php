<?php
include_once('../globales/conexion.php');
include_once('../globales/valida_usuario_auth.php');

$query = "SELECT id, nombre from usuarios
          order by nombre asc";

$ejecutaQuery = $conn->query($query);

while($fila = $ejecutaQuery->fetch_object()){
    $data[] = [
        "id" => $fila->id,
        "nombre" => $fila->nombre
    ];
}

$data = ['data' => $data];

echo json_encode($data);

?>