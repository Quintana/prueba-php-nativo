-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-01-2023 a las 01:44:56
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba_php_nativo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_generales`
--

CREATE TABLE `estados_generales` (
  `id` bigint(20) NOT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estados_generales`
--

INSERT INTO `estados_generales` (`id`, `descripcion`) VALUES
(1, 'ACTIVO'),
(2, 'INACTIVO'),
(3, 'INACTIVO POR CANTIDAD DE INTENTOS DE ACCESO INVALIDOS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `id` bigint(20) NOT NULL,
  `tabla_afectada` varchar(255) NOT NULL,
  `sql_ejecutado` text NOT NULL,
  `sql_revertir` text NOT NULL,
  `fecha_accion` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`id`, `tabla_afectada`, `sql_ejecutado`, `sql_revertir`, `fecha_accion`) VALUES
(1, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, 2023-01-26 17:10:41, \"WQQWEQWE\", \"FFGHFGH\");', 'delete from radicaciones where id = 5;', '2023-01-26 17:10:41'),
(2, 'radicaciones', 'update radicaciones set id_usuario = 2, nombre_solicitante = \"JHON DOE12\", fecha = 2023-01-26 17:32:44, asunto = \"ASUNTO DE PRUEBA\", texto_solicitud = \"ESTA ES UNA PRUEBA PARA EL TEXTO DE LA SOLICITUD\" ;', 'update radicaciones set id_usuario = 2, nombre_solicitante = \"JHON DOE\", fecha = \"2023-01-26 16:01:47\", asunto = \"ASUNTO DE PRUEBA\", texto_solicitud = \"ESTA ES UNA PRUEBA PARA EL TEXTO DE LA SOLICITUD\" ;', '2023-01-26 17:32:44'),
(3, 'radicaciones', 'delete from radicaciones where id = 2;', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 1\", \"2023-01-26 15:07:33\", \"ASUNTO\", \"TEXTO SOLICITUD\");', '2023-01-26 17:34:08'),
(4, 'radicaciones', 'update radicaciones set id_usuario = 2, nombre_solicitante = \"ASDASD\", fecha = 2023-01-26 18:11:55, asunto = \"WQQWEQWE\", texto_solicitud = \"FFGHFGH\" ;', 'update radicaciones set id_usuario = 2, nombre_solicitante = \"ASDASD\", fecha = \"2023-01-26 17:10:41\", asunto = \"WQQWEQWE\", texto_solicitud = \"FFGHFGH\" ;', '2023-01-26 18:11:55'),
(5, 'radicaciones', 'update radicaciones set id_usuario = 2, nombre_solicitante = \"JHON DOE12\", fecha = 2023-01-26 18:25:25, asunto = \"ASUNTO DE PRUEBA\", texto_solicitud = \"ESTA ES UNA PRUEBA PARA EL TEXTO DE LA SOLICITUD\" ;', 'update radicaciones set id_usuario = 2, nombre_solicitante = \"JHON DOE12\", fecha = \"2023-01-26 16:01:47\", asunto = \"ASUNTO DE PRUEBA\", texto_solicitud = \"ESTA ES UNA PRUEBA PARA EL TEXTO DE LA SOLICITUD\" ;', '2023-01-26 18:25:25'),
(6, 'radicaciones', 'update radicaciones set id_usuario = 2, nombre_solicitante = \"SOLICITANTE 3\", fecha = 2023-01-26 18:25:29, asunto = \"ASUNTO 3\", texto_solicitud = \"TEXTO\" ;', 'update radicaciones set id_usuario = 2, nombre_solicitante = \"SOLICITANTE 3\", fecha = \"2023-01-26 16:27:06\", asunto = \"ASUNTO 3\", texto_solicitud = \"TEXTO\" ;', '2023-01-26 18:25:29'),
(7, 'radicaciones', 'update radicaciones set id_usuario = 2, nombre_solicitante = \"SOLICITANTE 4\", fecha = 2023-01-26 18:25:33, asunto = \"ASUNTO\", texto_solicitud = \"TEXTO SOLICITUD\" ;', 'update radicaciones set id_usuario = 2, nombre_solicitante = \"SOLICITANTE 4\", fecha = \"2023-01-26 16:26:36\", asunto = \"ASUNTO\", texto_solicitud = \"TEXTO SOLICITUD\" ;', '2023-01-26 18:25:33'),
(8, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 5\", 2023-01-26 19:35:08, \"ASUNTO 5\", \"TEXTO DE LA SOLICITUD 5\");', 'delete from radicaciones where id = 6;', '2023-01-26 19:35:08'),
(9, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 6\", 2023-01-26 19:35:14, \"ASUNTO 6\", \"TEXTO DE LA SOLICITUD 6\");', 'delete from radicaciones where id = 7;', '2023-01-26 19:35:14'),
(10, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 7\", 2023-01-26 19:35:19, \"ASUNTO 7\", \"TEXTO DE LA SOLICITUD 7\");', 'delete from radicaciones where id = 8;', '2023-01-26 19:35:19'),
(11, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 8\", 2023-01-26 19:35:25, \"ASUNTO 8\", \"TEXTO DE LA SOLICITUD 8\");', 'delete from radicaciones where id = 9;', '2023-01-26 19:35:25'),
(12, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 9\", 2023-01-26 19:35:30, \"ASUNTO 9\", \"TEXTO DE LA SOLICITUD 9\");', 'delete from radicaciones where id = 10;', '2023-01-26 19:35:30'),
(13, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 10\", 2023-01-26 19:35:36, \"ASUNTO 10\", \"TEXTO DE LA SOLICITUD 10\");', 'delete from radicaciones where id = 11;', '2023-01-26 19:35:36'),
(14, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 11\", 2023-01-26 19:35:41, \"ASUNTO 11\", \"TEXTO DE LA SOLICITUD 11\");', 'delete from radicaciones where id = 12;', '2023-01-26 19:35:41'),
(15, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 12\", 2023-01-26 19:35:45, \"ASUNTO 12\", \"TEXTO DE LA SOLICITUD 12\");', 'delete from radicaciones where id = 13;', '2023-01-26 19:35:45'),
(16, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 13\", 2023-01-26 19:35:51, \"ASUNTO 13\", \"TEXTO DE LA SOLICITUD 13\");', 'delete from radicaciones where id = 14;', '2023-01-26 19:35:51'),
(17, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 14\", 2023-01-26 19:35:58, \"ASUNTO 14\", \"TEXTO DE LA SOLICITUD 14\");', 'delete from radicaciones where id = 15;', '2023-01-26 19:35:58'),
(18, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 15\", 2023-01-26 19:36:03, \"ASUNTO 15\", \"TEXTO DE LA SOLICITUD 15\");', 'delete from radicaciones where id = 16;', '2023-01-26 19:36:03'),
(19, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 16\", 2023-01-26 19:36:09, \"ASUNTO 16\", \"TEXTO DE LA SOLICITUD 16\");', 'delete from radicaciones where id = 17;', '2023-01-26 19:36:09'),
(20, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 17\", 2023-01-26 19:36:14, \"ASUNTO 17\", \"TEXTO DE LA SOLICITUD 17\");', 'delete from radicaciones where id = 18;', '2023-01-26 19:36:14'),
(21, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 18\", 2023-01-26 19:36:19, \"ASUNTO 18\", \"TEXTO DE LA SOLICITUD 18\");', 'delete from radicaciones where id = 19;', '2023-01-26 19:36:19'),
(22, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 19\", 2023-01-26 19:36:25, \"ASUNTO 19\", \"TEXTO DE LA SOLICITUD 19\");', 'delete from radicaciones where id = 20;', '2023-01-26 19:36:25'),
(23, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 20\", 2023-01-26 19:36:30, \"ASUNTO 20\", \"TEXTO DE LA SOLICITUD 20\");', 'delete from radicaciones where id = 21;', '2023-01-26 19:36:30'),
(24, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 21\", 2023-01-26 19:36:36, \"ASUNTO 21\", \"TEXTO DE LA SOLICITUD 21\");', 'delete from radicaciones where id = 22;', '2023-01-26 19:36:36'),
(25, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 22\", 2023-01-26 19:36:40, \"ASUNTO 22\", \"TEXTO DE LA SOLICITUD 22\");', 'delete from radicaciones where id = 23;', '2023-01-26 19:36:40'),
(26, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 23\", 2023-01-26 19:36:47, \"ASUNTO 23\", \"TEXTO DE LA SOLICITUD 23\");', 'delete from radicaciones where id = 24;', '2023-01-26 19:36:47'),
(27, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 24\", 2023-01-26 19:36:52, \"ASUNTO 24\", \"TEXTO DE LA SOLICITUD 24\");', 'delete from radicaciones where id = 25;', '2023-01-26 19:36:52'),
(28, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 25\", 2023-01-26 19:36:57, \"ASUNTO 25\", \"TEXTO DE LA SOLICITUD 25\");', 'delete from radicaciones where id = 26;', '2023-01-26 19:36:57'),
(29, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 26\", 2023-01-26 19:44:29, \"ASUNTO 26\", \"TEXTO DE LA SOLICITUD 26\");', 'delete from radicaciones where id = 27;', '2023-01-26 19:44:29'),
(30, 'radicaciones', 'insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (2, \"SOLICITANTE 27\", 2023-01-26 19:44:46, \"ASUNTO 27\", \"TEXTO DE LA SOLICITUD 27\");', 'delete from radicaciones where id = 28;', '2023-01-26 19:44:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `radicaciones`
--

CREATE TABLE `radicaciones` (
  `id` bigint(20) NOT NULL,
  `id_usuario` bigint(20) NOT NULL,
  `nombre_solicitante` varchar(255) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `asunto` varchar(255) NOT NULL,
  `texto_solicitud` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `radicaciones`
--

INSERT INTO `radicaciones` (`id`, `id_usuario`, `nombre_solicitante`, `fecha`, `asunto`, `texto_solicitud`) VALUES
(1, 2, 'JHON DOE12', '2022-01-12 16:01:47', 'ASUNTO DE PRUEBA', 'ESTA ES UNA PRUEBA PARA EL TEXTO DE LA SOLICITUD'),
(3, 2, 'SOLICITANTE 3', '2023-01-02 16:27:06', 'ASUNTO 3', 'TEXTO'),
(4, 2, 'SOLICITANTE 4', '2023-01-10 16:26:36', 'ASUNTO', 'TEXTO SOLICITUD'),
(5, 2, 'ASDASD', '2023-01-26 18:11:55', 'WQQWEQWE', 'FFGHFGH'),
(6, 2, 'SOLICITANTE 5', '2023-01-26 19:35:08', 'ASUNTO 5', 'TEXTO DE LA SOLICITUD 5'),
(7, 2, 'SOLICITANTE 6', '2023-01-26 19:35:14', 'ASUNTO 6', 'TEXTO DE LA SOLICITUD 6'),
(8, 2, 'SOLICITANTE 7', '2023-01-26 19:35:19', 'ASUNTO 7', 'TEXTO DE LA SOLICITUD 7'),
(9, 2, 'SOLICITANTE 8', '2023-01-26 19:35:25', 'ASUNTO 8', 'TEXTO DE LA SOLICITUD 8'),
(10, 2, 'SOLICITANTE 9', '2023-01-26 19:35:30', 'ASUNTO 9', 'TEXTO DE LA SOLICITUD 9'),
(11, 2, 'SOLICITANTE 10', '2023-01-26 19:35:36', 'ASUNTO 10', 'TEXTO DE LA SOLICITUD 10'),
(12, 2, 'SOLICITANTE 11', '2023-01-26 19:35:41', 'ASUNTO 11', 'TEXTO DE LA SOLICITUD 11'),
(13, 2, 'SOLICITANTE 12', '2023-01-26 19:35:45', 'ASUNTO 12', 'TEXTO DE LA SOLICITUD 12'),
(14, 2, 'SOLICITANTE 13', '2023-01-26 19:35:51', 'ASUNTO 13', 'TEXTO DE LA SOLICITUD 13'),
(15, 2, 'SOLICITANTE 14', '2023-01-26 19:35:58', 'ASUNTO 14', 'TEXTO DE LA SOLICITUD 14'),
(16, 2, 'SOLICITANTE 15', '2023-01-26 19:36:03', 'ASUNTO 15', 'TEXTO DE LA SOLICITUD 15'),
(17, 2, 'SOLICITANTE 16', '2023-01-26 19:36:09', 'ASUNTO 16', 'TEXTO DE LA SOLICITUD 16'),
(18, 2, 'SOLICITANTE 17', '2023-01-26 19:36:14', 'ASUNTO 17', 'TEXTO DE LA SOLICITUD 17'),
(19, 2, 'SOLICITANTE 18', '2023-01-26 19:36:19', 'ASUNTO 18', 'TEXTO DE LA SOLICITUD 18'),
(20, 2, 'SOLICITANTE 19', '2023-01-26 19:36:25', 'ASUNTO 19', 'TEXTO DE LA SOLICITUD 19'),
(21, 2, 'SOLICITANTE 20', '2023-01-26 19:36:30', 'ASUNTO 20', 'TEXTO DE LA SOLICITUD 20'),
(22, 2, 'SOLICITANTE 21', '2023-01-26 19:36:36', 'ASUNTO 21', 'TEXTO DE LA SOLICITUD 21'),
(23, 2, 'SOLICITANTE 22', '2023-01-26 19:36:40', 'ASUNTO 22', 'TEXTO DE LA SOLICITUD 22'),
(24, 2, 'SOLICITANTE 23', '2023-01-26 19:36:47', 'ASUNTO 23', 'TEXTO DE LA SOLICITUD 23'),
(25, 2, 'SOLICITANTE 24', '2023-01-26 19:36:52', 'ASUNTO 24', 'TEXTO DE LA SOLICITUD 24'),
(26, 2, 'SOLICITANTE 25', '2023-01-26 19:36:57', 'ASUNTO 25', 'TEXTO DE LA SOLICITUD 25'),
(27, 2, 'SOLICITANTE 26', '2023-01-26 19:44:29', 'ASUNTO 26', 'TEXTO DE LA SOLICITUD 26'),
(28, 2, 'SOLICITANTE 27', '2023-01-26 19:44:46', 'ASUNTO 27', 'TEXTO DE LA SOLICITUD 27');

--
-- Disparadores `radicaciones`
--
DELIMITER $$
CREATE TRIGGER `after_delete_radicaciones` AFTER DELETE ON `radicaciones` FOR EACH ROW BEGIN
	insert into log (tabla_afectada, sql_ejecutado, sql_revertir, fecha_accion)
    values(
		'radicaciones',
        CONCAT( "delete from radicaciones where id = ", OLD.id, ";" ),
        CONCAT( "insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (",OLD.id_usuario,", """,OLD.nombre_solicitante,""", """,OLD.fecha,""", """,OLD.asunto,""", """,OLD.texto_solicitud,""");" ),
        now()
    );
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_insert_radicaciones` AFTER INSERT ON `radicaciones` FOR EACH ROW BEGIN
	insert into log (tabla_afectada, sql_ejecutado, sql_revertir, fecha_accion)
    values(
		'radicaciones',
        CONCAT( "insert into radicaciones(id_usuario, nombre_solicitante, fecha, asunto, texto_solicitud) values (",NEW.id_usuario,", """,NEW.nombre_solicitante,""", ",now(),", """,NEW.asunto,""", """,NEW.texto_solicitud,""");" ),
        CONCAT( "delete from radicaciones where id = ", NEW.id, ";" ),
        now()
    );
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_update_radicaciones` AFTER UPDATE ON `radicaciones` FOR EACH ROW BEGIN
	insert into log (tabla_afectada, sql_ejecutado, sql_revertir, fecha_accion)
    values(
		'radicaciones',
        CONCAT( "update radicaciones set id_usuario = ",NEW.id_usuario,", nombre_solicitante = """,NEW.nombre_solicitante,""", fecha = ",now(),", asunto = """,NEW.asunto,""", texto_solicitud = """,NEW.texto_solicitud,""" ;" ),
        CONCAT( "update radicaciones set id_usuario = ",OLD.id_usuario,", nombre_solicitante = """,OLD.nombre_solicitante,""", fecha = """,OLD.fecha,""", asunto = """,OLD.asunto,""", texto_solicitud = """,OLD.texto_solicitud,""" ;" ),
        now()
    );
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` bigint(20) NOT NULL,
  `id_estado_general` bigint(20) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `id_estado_general`, `nombre`, `email`, `password`) VALUES
(1, 1, 'DARWIN EDER QUINTANA CHALA', 'DARWINQUINTANA44@GMAIL.COM', '979791107a66bfc481782aa7589c06c7'),
(2, 1, 'ALLISON SOPHIA QUINTANA SANDOVAL', 'ALLISON@CORREO.COM', 'd06b78b7f730ba231f1109fc34f13e47'),
(3, 3, 'ERICK SANTIAGO BERMUDEZ SANDOVAL', 'ERICK@CORREO.COM', 'fdaedacf10697930ae8218e3781e233c'),
(4, 1, 'MAGDA LILIANA SANDOVAL BARBOSA', 'LILIANA@CORREO.COM', 'ade1cc347a5c541dcf403d3f13cc04e2');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `estados_generales`
--
ALTER TABLE `estados_generales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `radicaciones`
--
ALTER TABLE `radicaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `relacion2` (`id_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `relacion1` (`id_estado_general`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estados_generales`
--
ALTER TABLE `estados_generales`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `radicaciones`
--
ALTER TABLE `radicaciones`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `radicaciones`
--
ALTER TABLE `radicaciones`
  ADD CONSTRAINT `relacion2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `relacion1` FOREIGN KEY (`id_estado_general`) REFERENCES `estados_generales` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
