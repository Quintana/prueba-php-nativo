<?php
include_once('globales/conexion.php');
include_once('globales/valida_usuario_auth.php');
include_once('globales/librerias.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Prueba PHP Nativa</a>
        <form action="auth/logout.php" class="d-flex" role="search">
            <button class="btn btn-outline-info" type="submit">Salir</button>
        </form>
        </div>
    </div>
    </nav>

    <main class="container">
        <div class="bg-light p-5 rounded">
            <h1>Listado de radicaciones realizadas</h1>
            <div class="row mt-5 mb-5">

                <div class="row mt-2">
                    <div class="col-md-6">
                        <label for="exampleFormControlInput1" class="form-label">Rango de fecha</label>
                        <input type="email" class="form-control" type="text" name="rango_fecha" id="rango_fecha" value="" placeholder="Ingrese el rango de fecha" >
                    </div>
                    <div class="col-md-6">
                        <label for="exampleFormControlTextarea1" class="form-label">Nombre del Solicitante</label>
                        <input type="email" class="form-control" type="text" name="nombre_solicitante_filtro" id="nombre_solicitante_filtro" value="" placeholder="Nombre del solicitante" >
                    </div>
                    
                </div>
                <div class="row mt-2">
                    <div class="col-md-6">
                        <label for="exampleFormControlTextarea1" class="form-label">Asunto</label>
                        <input type="email" class="form-control" type="text" name="asunto_filtro" id="asunto_filtro" value="" placeholder="Asunto" >
                    </div>
                    <div class="col-md-6">
                        <label for="exampleFormControlTextarea1" class="form-label">Usuario quien creo y/o actualizo</label>
                        <select class="form-select" aria-label="Default select example" id="usuario_creo_filtro" name="usuario_creo_filtro"> </select>
                    </div>

                    
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary" id="aplicar_filtro">Aplicar filtro</button>
                        <button type="button" class="btn btn-secondary" id="valores_inciales">Cargar valores iniciales</button>
                    </div>
                </div>

            </div>
            <div class="row">
                <table id="listado" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Usuario Inserta</th>
                            <th>Solicitante</th>
                            <th>Fecha Radicación</th>
                            <th>Asunto</th>
                            <th>Texto de la solicitud</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </main>


    <!-- Modal -->
    <div class="modal fade" id="editarModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Modificar Datos</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Nombre Solicitante <span style="color: red">*</span></label>
                    <input type="email" class="form-control borrar" id="nombre_solicitante" name="nombre_solicitante" placeholder="Ingrese el nombre sel solicitante">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Asunto <span style="color: red">*</span></label>
                    <input type="text" class="form-control borrar" id="asunto" name="asunto" placeholder="Ingrese el asunto del documento">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Texto de la solicitud <span style="color: red">*</span></label>
                    <textarea class="form-control borrar" id="texto_solicitud" name="texto_solicitud" rows="3" placeholder="Ingrese el texto de la solicitud"></textarea>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="actualizar">Actualizar</button>
                </div>
            </div>
        </div>
    </div>

    <div data-bs-toggle="modal" class="form-group" data-bs-target="#crearModal">
        <a class="btn-flotante" style="cursor: pointer" data-bs-toggle="tooltip" data-bs-placement="top" title="Crear información">
            <i class="fas fa-plus"></i> Nuevo Registro
        </a>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="crearModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Crear Datos</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Nombre Solicitante <span style="color: red">*</span></label>
                    <input type="email" class="form-control borrar1" id="nombre_solicitante1" name="nombre_solicitante1" placeholder="Ingrese el nombre sel solicitante">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Asunto <span style="color: red">*</span></label>
                    <input type="text" class="form-control borrar1" id="asunto1" name="asunto1" placeholder="Ingrese el asunto del documento">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Texto de la solicitud <span style="color: red">*</span></label>
                    <textarea class="form-control borrar1" id="texto_solicitud1" name="texto_solicitud1" rows="3" placeholder="Ingrese el texto de la solicitud"></textarea>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="crear_registro">Crear registro</button>
                </div>
            </div>
        </div>
    </div>

    

    
</body>
</html>

<script>
    $(document).ready(function(){
        <?php if(isset($_SESSION['login'])){ unset($_SESSION['login']); ?>
            Swal.fire({
                icon: 'success',
                title: 'Excelente',
                text: 'Bienvenido, Se ha logueado con éxito a la plataforma',
            });
        <?php }?>

        let fecha_inicio = '';
        let fecha_fin = '';
        let nombre_solicitante_filtro = '';
        let asunto_filtro = '';
        let usuario_creo_filtro = '';

        // definimos el datepicker a utilizar para el rango de fechas
        $('#rango_fecha').daterangepicker({
            "startDate": false,
            "endDate": false,
            "autoApply": true,
        }, function(start, end, label) {
            fecha_inicio = start.format('YYYY-MM-DD');
            fecha_fin = end.format('YYYY-MM-DD');
        });

        $.ajax({
                method: "GET",
                url: "./consultas/usuarios.php"
            }).done(function( data ) {
                data = $.parseJSON(data);
                $('<option>').val('').text('Seleccione...').appendTo('#usuario_creo_filtro');
                $.each(data.data, function (idx, val) {
                    $('<option>').val(val.id).text(val.nombre).appendTo('#usuario_creo_filtro');
                });

            }).fail(function() {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'No se actualizaron los datos: por favor contacte al administrador del sistema',
                });
            });
        
        // metodo para listar los datos en el datatable
        function listarDatos(){
            // consultamos el listado de los registros de las radicaciones
            $('#listado').DataTable({
                processing: true,
                language: {
                    url: 'librerias/datatable_lang/es.json'
                },
                bDestroy: true,
                responsive: true,
                lengthMenu: [[25, 50, 100, 200], [25, 50, 100, 200]],
                order: [0, 'desc'],
                ajax: {
                    url: './consultas/radicaciones.php',
                    type: 'POST',
                    data: {
                        fecha_inicio : fecha_inicio,
                        fecha_fin : fecha_fin,
                        nombre_solicitante_filtro : nombre_solicitante_filtro,
                        asunto_filtro : asunto_filtro,
                        usuario_creo_filtro : usuario_creo_filtro,
                    }
                },
                columns: [
                    { data: 'id' },
                    { data: 'usuario_creador' },
                    { data: 'solicitante' },
                    { data: 'fecha' },
                    { data: 'asunto' },
                    { data: 'texto_solicitud' },
                    { className: "text-center", orderable: false },
                ],
                columnDefs: [
                    {
                        targets: 6,
                        data: function(row, type, val, meta){
                            let opciones = '';

                            if(type === 'display'){
                                opciones += `<div data-bs-toggle="modal" class="form-group editar" data-bs-target="#editarModal" data-val="${row.id}">
                                                <a style="cursor: pointer" data-val="${row.id}" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar información">
                                                <i class="fas fa-edit"></i>
                                                </a>
                                            </div>`;
                            }
                            return opciones;
                        }
                    }
                ]
            });
        }

        listarDatos();

        // cargar listado con filtros
        $(document).on('click', '#aplicar_filtro', function(){
            nombre_solicitante_filtro = $('#nombre_solicitante_filtro').val();
            asunto_filtro = $('#asunto_filtro').val();
            usuario_creo_filtro = $('#usuario_creo_filtro').val();
            listarDatos();
        });

        // cargar listado inicial sin filtros
        $(document).on('click', '#valores_inciales', function(){
            fecha_inicio = "";
            fecha_fin = "";
            nombre_solicitante_filtro = "";
            asunto_filtro = "";
            usuario_creo_filtro = "";
            listarDatos();
        });

        let id_radicacion = '';

        $(document).on('click', '.editar', function(){
            id_radicacion = $(this).data('val');
            $('.borrar').val('');

            $.ajax({
                method: "POST",
                url: "./consultas/radicaciones_por_id.php",
                data: { 
                    id_radicacion: id_radicacion
                }
            }).done(function( data ) {
                data = $.parseJSON(data);
                $('#nombre_solicitante').val(data.nombre_solicitante);
                $('#asunto').val(data.asunto);
                $('#texto_solicitud').val(data.texto_solicitud);
            }).fail(function() {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'No se actualizaron los datos: por favor contacte al administrador del sistema',
                });
            });
        });

        // consultamos el id para modificar los datos
        $(document).on('click', '#actualizar', function(){

            let nombre_solicitante = $('#nombre_solicitante').val();
            let asunto = $('#asunto').val();
            let texto_solicitud = $('#texto_solicitud').val();

            if(!id_radicacion){
                Swal.fire({
                    icon: 'info',
                    title: 'Información',
                    text: 'Usuario no seleccionado',
                });
                return;
            }

            if (!nombre_solicitante || !asunto || !texto_solicitud){
                Swal.fire({
                    icon: 'info',
                    title: 'Información',
                    text: 'Todos los datos son obligatorios',
                });
                return;
            }

            $.ajax({
                method: "POST",
                url: "./consultas/crear_modificar_radicaciones.php",
                data: { 
                    accion: 'actualizar',
                    id_radicacion: id_radicacion, 
                    nombre_solicitante: nombre_solicitante, 
                    asunto: asunto, 
                    texto_solicitud: texto_solicitud 
                }
            }).done(function( data ) {
                data = $.parseJSON(data);
                if(data.status == 200){
                    Swal.fire({
                        icon: 'success',
                        title: 'Excelente',
                        text: data.data,
                    });
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Upsss..',
                        text: data.data,
                    });
                }
                listarDatos();
            }).fail(function() {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'No se actualizaron los datos: por favor contacte al administrador del sistema',
                });
            });
        });

        // consultamos el id para crear los datos
        $(document).on('click', '#crear_registro', function(){

            let nombre_solicitante = $('#nombre_solicitante1').val();
            let asunto = $('#asunto1').val();
            let texto_solicitud = $('#texto_solicitud1').val();

            if (!nombre_solicitante || !asunto || !texto_solicitud){
                Swal.fire({
                    icon: 'info',
                    title: 'Información',
                    text: 'Todos los datos son obligatorios',
                });
                return;
            }

            $.ajax({
                method: "POST",
                url: "./consultas/crear_modificar_radicaciones.php",
                data: { 
                    accion: 'insertar',
                    nombre_solicitante: nombre_solicitante, 
                    asunto: asunto, 
                    texto_solicitud: texto_solicitud 
                }
            }).done(function( data ) {
                data = $.parseJSON(data);
                if(data.status == 200){
                    Swal.fire({
                        icon: 'success',
                        title: 'Excelente',
                        text: data.data,
                    });
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Upsss..',
                        text: data.data,
                    });
                }
                listarDatos();
            }).fail(function() {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'No se actualizaron los datos: por favor contacte al administrador del sistema',
                });
            });
        });
    });
</script>